# Client WebSocket

Questo è un client WebSocket di base che si connette a un server WebSocket e invia e riceve messaggi tramite una pagina web.

## Utilizzo

Per utilizzare il client WebSocket, aprire il file `index.html` in un browser web.

Per ulteriori informazioni sull'utilizzo del client WebSocket, consultare il codice sorgente del file `index.html`.

## Configurazione

Modificare l'indirizzo del server WebSocket nel file `index.js`, sostituendo `localhost:8080` con l'indirizzo corretto del server.

## Descrizione

Il client WebSocket utilizza il linguaggio di markup HTML, il foglio di stile CSS e il linguaggio di scripting JavaScript per creare una pagina web che si connette a un server WebSocket e invia e riceve messaggi.

Per la gestione della connessione WebSocket, il client utilizza le Web API del WebSocket.

Per ulteriori informazioni sull'utilizzo del client WebSocket, consultare la documentazione disponibile [qui](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket).

## Licenza

Questo software è rilasciato sotto la licenza MIT.
